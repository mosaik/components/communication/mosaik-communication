from setuptools import setup

setup(
    name='mosaik-communication',
    version='0.1',
    author='Thomas Raub',
    author_email='mosaik at offis.de',
    description=('A basic communication module for mosaik.'),
    long_description=(open('README.rst').read() + '\n\n' +
                      open('CHANGES.txt').read() + '\n\n' +
                      open('AUTHORS.txt').read()),
    url='https://gitlab.com/mosaik/components/communication/mosaik-communication',
    py_modules=['mosaik_communication'],
    install_requires=[
        'mosaik-api>=2.4',
        'mosaik>=2.6'
    ],
    include_package_data=True,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU Lesser General Public License v2 (LGPLv2)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Scientific/Engineering',
    ],
    )
