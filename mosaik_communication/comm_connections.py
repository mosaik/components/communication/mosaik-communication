import json


class CommConnections():
    def __init__(self, world):
        world.sim_config['CommSim'] = {
            'python': 'mosaik_communication.comm_simulator:CommSimulator'}
        self.world = world
        self.nodes = []
        self.connections = {}
        self.node_attributes = []
        self.connection_connections = {}
        self.scenario_connections = set()
        self.connected_sims = set()

    def connect_comm(self, src, dest, *attr_pairs, delay=0, offset=None,
                     factor=None, **kwargs):
        #assert set(kwargs.keys()) <= {'public', 'name'}, "Only 'public' and " \
        #            "'name' are allowed as key-word arguments for connections."
        assert kwargs == {}, 'No arguments are allowed for comm. connections.'

        # Add src and dest as network nodes:
        is_weaks = [None, None]
        for i, full_id in enumerate([src.full_id, dest.full_id]):
            if full_id not in self.nodes:
                self.nodes.append(full_id)

            sid = full_id.split('.')[0]
            if sid in self.connected_sims:
                is_weaks[i] = True
            else:
                is_weaks[i] = False
                self.connected_sims.add(sid)

        connection_name = f"{src.full_id}->{dest.full_id}"
        if connection_name not in self.connections:
            # If only delay is defined, put it as integer (as shortcut), else
            # add properties as dictionary:
            if offset is None and factor is None:
                props = delay
            else:
                props = {'delay': delay, 'offset': offset, 'factor': factor}
            self.connections[connection_name] = props
        else:
            raise RuntimeError(
                'You can only create one connection per entity pair for now.')

        # Expand single attributes "attr" to ("attr", "attr") tuples:
        attr_pairs = tuple(
            (a, a) if type(a) is str else a for a in attr_pairs)
        self.scenario_connections.add((src, dest, attr_pairs, tuple(is_weaks)))
        self.node_attributes.extend([attr_pair[0] for attr_pair in attr_pairs])

        return connection_name

    def connect_to_connection(self, src, name, *attr_pairs):
        attr_pairs = tuple((a, a) if type(a) is str else a for a in attr_pairs)
        self.connection_connections[name] = (src, attr_pairs)

    def start_simulator(self):
        network_config = {'nodes': self.nodes, 'connections': self.connections,
                          'attributes': self.node_attributes}
        with open('network_config_auto.json', 'w') as outfile:
            json.dump(network_config, outfile)

        comm_entity = self.world.start('CommSim', config_file='network_config_auto.json').CommModel()

        for src, dest, attr_pairs, is_weaks in self.scenario_connections:
            input_attrs = [attr_pair[0] for attr_pair in attr_pairs]
            comm_child = [e for e in comm_entity.children
                          if e.eid == src.full_id][0]
            self.world.connect(src, comm_child, *input_attrs, weak=is_weaks[0])
            comm_child = [e for e in comm_entity.children
                          if e.eid == dest.full_id][0]
            self.world.connect(comm_child, dest, *attr_pairs, weak=is_weaks[1])

        for name, connections in self.connection_connections.items():
            comm_child = [e for e in comm_entity.children if e.eid == name][0]
            src, attr_pairs = connections
            self.world.connect(src, comm_child, *attr_pairs)

        return comm_entity
