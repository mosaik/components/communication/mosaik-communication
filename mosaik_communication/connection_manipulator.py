"""

"""

import mosaik_api


META = {
    'type': 'event-based',
    'models': {
        'Manipulator': {
            'public': True,
            'any_inputs': False,
            'params': ['delays', 'factors', 'offsets'],
            'attrs': ['delay', 'factor', 'offset'],
        },
    },
}


class Manipulator(mosaik_api.Simulator):
    def __init__(self):
        super().__init__(META)
        self.eid = None
        self.delays = None
        self.factors = None
        self.offsets = None
        self.current_delay = None
        self.current_factor = None
        self.current_offset = None

    def init(self, sid, time_resolution):
        return self.meta

    def create(self, num, model, delays={0: 0}, factors={}, offsets={}):
        if num > 1 or self.eid is not None:
            raise RuntimeError('Can only create one instance of Monitor.')

        if 0 not in delays:
            raise RuntimeError('Delay value for time 0 needs to be provided.')

        self.eid = 'Manipulator'
        self.delays = delays
        self.factors = factors
        self.offsets = offsets
        return [{'eid': self.eid, 'type': model}]

    def step(self, time, inputs, max_advance):
        self.current_delay = self.delays.pop(time, None)
        self.current_factor = self.factors.pop(time, None)
        self.current_offset = self.offsets.pop(time, None)
        next_steps = [list(manip.keys())[0] for manip in [self.delays, self.factors, self.offsets] if manip]
        if next_steps:
            next_step = min(next_steps)
        else:
            next_step = None
        return next_step

    def get_data(self, outputs):
        data = {}
        if self.current_delay is not None:
            data['delay'] = self.current_delay
        if self.current_factor is not None:
            data['factor'] = self.current_factor
        if self.current_offset is not None:
            data['offset'] = self.current_offset

        return {self.eid: data}


if __name__ == '__main__':
    mosaik_api.start_simulation(Manipulator())