"""
TODO
"""
import json
import mosaik_api
import heapq as hq


META = {
    'type': 'event-based',
    'models': {
        'CommModel': {
            'public': True,
            'any_inputs': False,
            'params': [],
            'attrs': [],
        },
        'node': {
            'public': False,
            'any_inputs': True,
            'params': [],
            'attrs': ['message'],
        },
        'connection': {
            'public': False,
            'any_inputs': False,
            'params': [],
            'attrs': ['delay', 'factor', 'offset'],
        },
    },
}


class CommSimulator(mosaik_api.Simulator):
    def __init__(self):
        super().__init__(META)
        self.sid = None
        self.eid = None
        self.children = []
        self.connections = {}
        self.connection_eids = set()

        self.event_queue = None
        self.data = None
        self.output_time = None

    def init(self, sid, time_resolution, config_file):
        self.sid = sid

        with open(config_file, "r") as jsonfile:
            config = json.load(jsonfile)

        for node in config['nodes']:
            self.children.append({'eid': node, 'type': 'node', 'rel': [], })

        for name, props in config['connections'].items():
            src, dest = name.split('->')
            if isinstance(props, int):
                props = {'delay': props, 'factor': None, 'offset': None}
            self.connection_eids.add(name)
            self.connections[src] = {dest: props}
            self.children.append(
                {'eid': name, 'type': 'connection', 'rel': [], })

        if 'attributes' in config:
            self.meta['models']['node']['attrs'].extend(config['attributes'])

        return self.meta

    def create(self, num, model):
        if num > 1 or self.eid is not None:
            raise RuntimeError('Can only create one instance of CommSimulator.')

        self.eid = 'CommSim'
        self.event_queue = []

        comm_entity = {'eid': self.eid,
                       'type': 'CommModel',
                       'rel': [],
                       'children': self.children,
                       }

        return [comm_entity]

    def step(self, time, inputs, max_advance):
        # Check for new connection properties first:
        for eid in self.connection_eids:
            attrs = inputs.pop(eid, {})
            if attrs:
                for attr, value_dict in attrs.items():
                    src, dest = eid.split('->')
                    assert len(value_dict) == 1
                    value = list(value_dict.values())[0]
                    self.connections[src][dest][attr] = value

        # Then treat normal input:
        for eid, attrs in inputs.items():
            for attr, value_dict in attrs.items():
                for src_full_id, value in value_dict.items():  # TODO: Only one value makes sense here, right?
                    for dest, props in self.connections[eid].items():
                        delay = props['delay']
                        factor = props['factor']
                        if factor is not None:
                            value = value * factor
                        offset = props['offset']
                        if offset is not None:
                            value = value + offset
                        # Messages are only queued for positive delays, which
                        # allows message loss via setting negative delays.
                        if delay >= 0:
                            arrival_time = time + delay
                            hq.heappush(self.event_queue,
                                        (arrival_time, dest, attr, value))

        self.data = {}
        if self.event_queue and self.event_queue[0][0] <= max_advance:
            # As we can only provide output for one time per step, only
            # take events for the first occurring time:
            output_time = self.output_time = self.event_queue[0][0]
            while self.event_queue and self.event_queue[0][0] == output_time:
                _, eid, attr, value = hq.heappop(self.event_queue)
                self.data.setdefault(eid, {})[attr] = value

        # If there are events left, request a new step:
        if self.event_queue:
            next_step = self.event_queue[0][0]
        else:
            next_step = None

        return next_step

    def get_data(self, outputs):
        data = {}
        for eid, attrs in self.data.items():
            data[eid] = {}
            for attr, val in attrs.items():
                data[eid][attr] = val

        if data:
            data['time'] = self.output_time

        return data

    def finalize(self):
        pass


if __name__ == '__main__':
    mosaik_api.start_simulation(CommSimulator())
