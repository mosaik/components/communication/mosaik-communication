from mosaik import scenario
from execution_graph_tools import plot_execution_graph_st as plot_execution_graph


sim_config = {
    'TestSim': {'python': 'simulators.generic_test_simulator:TestSim'},
    'Collector': {'python': 'simulators.collector:Collector'},
    'CommSim': {'python': 'mosaik_communication.comm_simulator:CommSimulator'},
}


world = scenario.World(sim_config, debug=True)

comm_entity = world.start('CommSim', config_file='network_config_basic.json').CommModel()

test_model = world.start('TestSim', step_type='event-based',
                          self_steps={0: 1, 1: 4},
                          output_timing={1: 1}).A()
world.set_event(test_model.sid)

collector = world.start('Collector').Monitor()

comm_nodes = {e.eid: e for e in comm_entity.children if e.type == 'node'}

world.connect(test_model, comm_nodes['node1'], 'val_out')
world.connect(comm_nodes['node2'], collector, 'val_out')

world.run(until=5)

plot_execution_graph(world)
