import os
import sys

from mosaik import scenario
from mosaik_communication.comm_connections import CommConnections

sys.path.insert(0, os.getcwd())


sim_config = {
    'TestSim': {'python': 'simulators.generic_test_simulator:TestSim'},
    'Manipulator': {'python': 'mosaik_communication.connection_manipulator:Manipulator'},
    'Collector': {'python': 'simulators.collector:Collector'},
}


world = scenario.World(sim_config, debug=False)

test_model = world.start('TestSim', step_type='event-based',
                          self_steps=dict(zip(range(5), range(1, 6))),
                          output_timing={2: 2}).A()
world.set_event(test_model.sid)

collector = world.start('Collector')
monitor = collector.Monitor()

manipulator = world.start('Manipulator').Manipulator(delays={0: 0, 2: 2})
world.set_event(manipulator.sid)

# Connect entities
world.connect(test_model, monitor, 'val_out')

comm_connections = CommConnections(world)
conn1 = comm_connections.connect_comm(test_model, monitor, 'val_out', delay=1)
comm_connections.connect_to_connection(manipulator, conn1, 'delay')
comm_entity = comm_connections.start_simulator()

world.run(until=5)
