from mosaik import scenario


sim_config = {
    'CommSim': {'python': 'mosaik_communication.comm_simulator:CommSimulator'},
    'TestSim': {'python': 'simulators.generic_test_simulator:TestSim'},
    'Manipulator': {'python': 'mosaik_communication.connection_manipulator:Manipulator'},
    'Collector': {'python': 'simulators.collector:Collector'},
}


world = scenario.World(sim_config, debug=False)

comm_entity = world.start('CommSim', config_file='network_config.json').CommModel()

test_model = world.start('TestSim', step_type='event-based',
                          self_steps=dict(zip(range(5), range(1, 6))),
                          output_timing={2: 2}).A()
world.set_event(test_model.sid)

collector = world.start('Collector').Monitor()

manipulator = world.start('Manipulator').Manipulator(delays={0: 0, 2: 2}, offsets={2: 0.1})

# Connect entities
#world.connect(test_model, monitor, 'val_out')

comm_nodes = {e.eid: e for e in comm_entity.children if e.type == 'node'}
comm_connections = {e.eid: e for e in comm_entity.children if e.type == 'connection'}

world.connect(test_model, comm_nodes['node1'], 'val_out')
world.connect(comm_nodes['node2'], collector, 'val_out')

world.connect(manipulator, comm_connections['node1->node2'], 'delay', 'offset')

world.run(until=5)
