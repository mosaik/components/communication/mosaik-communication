mosaik-communication
====================

This is a basic communication suite for mosaik using delays.

Installation
------------

::

    $ pip install git+https://gitlab.com/mosaik/mosaik-communication.git

Example scenario
-----

You can find an example scenario in examples/comm_scenario.py. You can execute it with::

    $ git clone https://gitlab.com/mosaik/mosaik-communication
    $ cd mosaik-communication
    $ pip install -r requirements.txt
    $ pip install -e .
    $ cd examples
    $ python comm_scenario.py

